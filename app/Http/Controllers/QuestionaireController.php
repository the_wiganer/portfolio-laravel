<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\questionaire;
class QuestionaireController extends Controller
{
    /*
    * Secure the set of pages to the admin.
    */
    public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $questionaire = questionaire::all();
      return view('admin.questionaire.show',['questionaires' =>$questionaire]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.questionaire.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     /**
     *Store function will store the data that is created by the cretae function
     *and put it into the Database
     *The redirects it to the questionaire page
     */
    public function store(Request $request)
    {
        $input = $request->all();
        questionaire::create($input);
        return redirect('questionaire');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     /**
     *Edit function will use the data from $questionaire and return to
     *original queestionaire view for the admins
     */
    public function edit($id)
    {
      $questionaire = questionaire::findOrFail($id);
      return view('admin.questionaire.edit', compact('questionaire'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $questionaire = questionaire::findOrFail($id);
      $questionaire->update($request->all());
      return redirect('admin.questionaire.edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionaire = questionaire::find($id);
        $questionaire->delete();
        return redirect('/questionaire');
    }
}
