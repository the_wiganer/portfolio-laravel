<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questionaire extends Model
{
  protected $fillable =[
    'id',
    'questions',
    'answers',
  ];

  //Code for this line found at https://stackoverflow.com/questions/28277955/laravelunknown-column-updated-at
  //Last used 27/04/18
  public $timestamps = false;
}
