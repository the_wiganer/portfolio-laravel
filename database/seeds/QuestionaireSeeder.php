<?php

use Illuminate\Database\Seeder;

class QuestionaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('questionaires')->insert([
        ['id' =>1, 'questions'=>' Do you find closed questions hard to answer', 'answers'=> 1],
      ]);
    }
}
