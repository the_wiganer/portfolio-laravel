<!doctype html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Admin - create questrions</title>
    <link rel="stylesheet" href="/css/app.css" />
  </head>
  <body>
    <div class="container">
      <header class="row">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <ul class="nav navbar-nav">
                  <li><a class="navbar-brand" href="/questionaire">Admin</a></li>
                  <li class="active"><a href="/questionaire/create">Create</a></li>
                </ul>
            </div>
        </nav>
      </header>
      <article class ="row">
        <h1>Add a new question</h1>
        <!-- The code area for the form being used to create the new question-->
        {!! Form::open(['url' =>'questionaire']) !!}
          <div class="form-group">
            <!--This is the part of the form where the admin can add questions for the user-->
            {!! Form::label('questions', 'Add a question:') !!}
            {!! Form::text('questions', null, ['class' => 'form-control']) !!}
          </div>
          <div class="form-group">
            <!--Submission button -->
            {!! Form::submit('Submit', ['class' => 'btn btn-primary form-control']) !!}
          </div>
        {!! Form::close() !!}
      </article>
    </div>
  </body>
</html>
