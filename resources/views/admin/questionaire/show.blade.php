<html>
  <head>
    <meta charset="UTF-8">
    <title>Admin - create questrions</title>
    <link rel="stylesheet" href="/css/app.css" />
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
         <div class="container-fluid">
              <ul class="nav navbar-nav">
                 <li class="active"><a href="/questionaire">Admin</a></li>
                 <li><a href="/questionaire/create">Create</a></li>
                 <li><a href="/questionaire/edit">Edit</a></li>

             </ul>
         </div>
     </nav>

    <h1>Admin- View Questionaire</h1>
    <table class="table table-striped table-bordered">
      <thead>
      <tr>
        <td>Question</td>
        <td>Answer</td>
      </tr>
      </thead>
      <tbody>
      @foreach ($questionaires as $questionaire)
        <tr>
          <td>{{$questionaire->questions}}</td>
          <td>{{$questionaire->answers}}</td>
          <td>
            <td>
              {!! Form::open(['method' => 'DELETE', 'route' => ['questionaire.destroy', $questionaire->id]]) !!}
              {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
              {!! Form::close() !!}

              </td>
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </body>
</html>
