<html>
  <head>
    <meta charset="UTF-8">
    <title>User- Answer questions</title>
    <link rel="stylesheet" href="/css/app.css" />
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
         <div class="container-fluid">
              <ul class="nav navbar-nav">
                 <li class="active"><a href="/">Home</a></li>
             </ul>
         </div>
     </nav>
    <h1>View Questionnaire</h1>
    <!--Here a table has been used to display the data for the users to see-->
    <!--There is a form within the table to get the users information from-->
    <!--This helps keep the data organised and easy for the user to seee-->
    <table class="table table-striped table-bordered">
      <thead>
      <tr>
        <td>Question</td>
        <td>Answer</td>
      </tr>
      </thead>
      <tbody>
      <!--Uses the data from the questionaire data-->
      @foreach ($questionaires as $questionaire)
        <tr>
          <td>{{$questionaire->questions}}</td>
          <td>
            {!! Form::model($questionaire, ['method' => 'PATCH', 'url' =>'questionaire/' .$questionaire->id])!!}
            <div class="form-group">
                {!! Form::label('answers', '') !!}
                {!! Form::select('answers', array('1' => 'Strongly agree', '2' => 'Agree', '3' => 'No opinion', '4' => 'Diagree', '5' => 'Strongly disgaree'), null,['placeholder' => 'Select...']) !!}
            </div>
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>
    <!--The submition button appear outside of the tabel so that each field doesn't have it's own submission-->
    <div class="form-group">
        {!! Form::submit('Submit', ['class' => 'btn btn-primary form-control']) !!}
    </div>
    {!! Form::close() !!}
  </body>
</html>
